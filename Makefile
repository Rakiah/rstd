# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/06/29 00:10:08 by Rakiah            #+#    #+#              #
#    Updated: 2016/08/04 23:12:01 by Rakiah           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = librstd.a

CC = gcc
CFLAGS = -Wall -Wextra -Werror
OPTIMIZE = yes
PROF = no
DEBUG = no

# Paths
PATH_HEADERS = includes
PATH_SRC = sources
PATH_OBJ = objects

CFLAGS += -I $(PATH_HEADERS)

# Debug
ifeq ($(DEBUG), yes)
	CFLAGS += -g -O0
endif

# Prof
ifeq ($(PROF), yes)
	CFLAGS += -pg
endif

# Optimization
ifeq ($(OPTIMIZE), yes)
	CFLAGS += -O3
endif

vpath %.c $(PATH_SRC)

# Sources
SOURCES += rstd_isupper.c
SOURCES += rstd_memjoin.c
SOURCES += rstd_strclr.c
SOURCES += rstd_strnew.c
SOURCES += rstd_itoa.c
SOURCES += rstd_memmove.c
SOURCES += rstd_strcmp.c
SOURCES += rstd_strnstr.c
SOURCES += rstd_memset.c
SOURCES += rstd_strcpy.c
SOURCES += rstd_strrchr.c
SOURCES += rstd_atoi.c
SOURCES += rstd_atof.c
SOURCES += rstd_memswap.c
SOURCES += rstd_strdel.c
SOURCES += rstd_strrev.c
SOURCES += rstd_bzero.c
SOURCES += rstd_strdup.c
SOURCES += rstd_strndup.c
SOURCES += rstd_strsplit.c
SOURCES += rstd_strequ.c
SOURCES += rstd_strstr.c
SOURCES += rstd_striter.c
SOURCES += rstd_strsub.c
SOURCES += rstd_putchar.c
SOURCES += rstd_striteri.c
SOURCES += rstd_strtolower.c
SOURCES += rstd_putchar_fd.c
SOURCES += rstd_strjoin.c
SOURCES += rstd_strtoupper.c
SOURCES += rstd_putendl.c
SOURCES += rstd_strlcat.c
SOURCES += rstd_strtrim.c
SOURCES += rstd_getnextxchar.c
SOURCES += rstd_putendl_fd.c
SOURCES += rstd_strlcpy.c
SOURCES += rstd_isalnum.c
SOURCES += rstd_putnbr.c
SOURCES += rstd_strlen.c
SOURCES += rstd_strtrimchar.c
SOURCES += rstd_isalpha.c
SOURCES += rstd_memalloc.c
SOURCES += rstd_putnbr_fd.c
SOURCES += rstd_strmap.c
SOURCES += rstd_strtrimlchar.c
SOURCES += rstd_isascii.c
SOURCES += rstd_memccpy.c
SOURCES += rstd_putstr.c
SOURCES += rstd_strmapi.c
SOURCES += rstd_strtrimrchar.c
SOURCES += rstd_isdigit.c
SOURCES += rstd_memchr.c
SOURCES += rstd_putstr_fd.c
SOURCES += rstd_strncat.c
SOURCES += rstd_tolower.c
SOURCES += rstd_islower.c
SOURCES += rstd_memcmp.c
SOURCES += rstd_strcat.c
SOURCES += rstd_strncmp.c
SOURCES += rstd_toupper.c
SOURCES += rstd_isprint.c
SOURCES += rstd_memcpy.c
SOURCES += rstd_strchr.c
SOURCES += rstd_strncpy.c
SOURCES += rstd_isspace.c
SOURCES += rstd_memdel.c
SOURCES += rstd_strchr_count.c
SOURCES += rstd_strnequ.c
SOURCES += rstd_strchrnul.c
SOURCES += rstd_strnchr.c
SOURCES += rstd_create_tab.c
SOURCES += rstd_delete_tab.c
SOURCES += rstd_read_lines.c

INCLUDES += rstd.h

# Headers
HEADERS += $(addprefix $(PATH_HEADERS)/, $(INCLUDES))

# Objects
OBJECTS += $(addprefix $(PATH_OBJ)/, $(SOURCES:%.c=%.o))

all: $(NAME)

$(NAME): $(OBJECTS)
	@ar rcs $(NAME) $(OBJECTS)
	@echo library linked correctly

$(OBJECTS): $(HEADERS) | $(PATH_OBJ)
$(OBJECTS): $(PATH_OBJ)/%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<

$(PATH_OBJ):
	@-mkdir -p $@

clean:
	@rm -rf $(PATH_OBJ)
	@echo removed binary files

fclean: clean
	@rm -f $(NAME)
	@echo removed library

re: fclean all

.PHONY: all clean fclean re
