/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_strncmp.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:15 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/29 00:42:49 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

int	rstd_strncmp(const char *s1, const char *s2, size_t length)
{
	while (*s1 == *s2 && --length > 0)
	{
		if (*s1 == '\0')
			return (0);
		(s1)++;
		(s2)++;
	}
	return (*(unsigned char *)s1) - (*(unsigned char *)s2);
}
