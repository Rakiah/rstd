/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_strlcpy.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:15 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/28 23:45:39 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

size_t	rstd_strlcpy(char *dest, const char *src, size_t size)
{
	unsigned int	destlength;
	unsigned int	diff;
	unsigned int	i;

	destlength = rstd_strlen(dest);
	diff = size - destlength - 1;
	i = 0;
	while (src[i] && diff > 0)
	{
		dest[i] = src[i];
		diff--;
		i++;
	}
	dest[destlength + i] = '\0';
	return (i);
}
