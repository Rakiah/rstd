/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_atoi.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:10 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/28 23:45:34 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

int		rstd_atoi(const char *str)
{
	int		is_negative;
	int		result;

	while (rstd_isspace((int)*str))
		(str)++;
	result = 0;
	is_negative = 0;
	if (*str == '-')
	{
		is_negative = 1;
		(str)++;
	}
	else if (*str == '+')
		(str)++;
	while (rstd_isdigit((int)*str))
	{
		result = ((result * 10) + (*str - '0'));
		(str)++;
	}
	result *= (is_negative == 1 ? -1 : 1);
	return (result);
}
