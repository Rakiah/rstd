/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_putnbr_fd.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:13 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/28 23:45:36 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

void	rstd_putnbr_fd(int nb, int fd)
{
	char	tab[10];
	int		lengthtab;
	char	digit;

	if (nb == 0)
		rstd_putchar_fd('0', fd);
	if (nb < 0)
		rstd_putchar_fd('-', fd);
	digit = 0;
	lengthtab = 0;
	while (nb != 0)
	{
		digit = (nb % 10);
		if (nb < 0)
			digit *= -1;
		tab[lengthtab] = digit + '0';
		lengthtab++;
		nb /= 10;
	}
	lengthtab--;
	while (lengthtab >= 0)
		rstd_putchar_fd(tab[lengthtab--], fd);
}
