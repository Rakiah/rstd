/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_strjoin.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:15 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/29 00:40:52 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

char	*rstd_strjoin(char const *s1, char const *s2)
{
	char	*str_new;
	size_t	len;

	len = rstd_strlen(s1) + rstd_strlen(s2);
	str_new = rstd_strnew(len);
	rstd_strcat(rstd_strcat(str_new, s1), s2);
	return (str_new);
}
