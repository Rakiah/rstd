/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_strtrimrchar.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:17 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/29 00:46:33 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

char		*rstd_strtrimrchar(char const *s, int rm)
{
	char		*str_new;
	int			end;

	end = rstd_getnextxchar(s, rm, rstd_strlen(s) - 1, -1) + 1;
	str_new = rstd_strnew(end);
	rstd_strncpy(str_new, s, end);
	return (str_new);
}
