/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_memcmp.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:11 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/29 00:37:24 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

static int	rstd_memcmp_align(const void **s1, const void **s2, size_t *n)
{
	unsigned char	*str1;
	unsigned char	*str2;
	size_t			size;

	str1 = (unsigned char *)*s1;
	str2 = (unsigned char *)*s2;
	size = (unsigned long)str1 % sizeof(void *);
	if (size > *n)
		size = *n;
	*n -= size;
	while (size--)
	{
		if (*str1 != *str2)
			return (*str1 - *str2);
		++str1;
		++str2;
	}
	*s1 = (void*)str1;
	*s2 = (void*)str2;
	return (0);
}

static int	rstd_memcmp_bulk(const void **s1, const void **s2, size_t *n)
{
	unsigned long	*str1;
	unsigned long	*str2;
	int				i;

	str1 = (unsigned long *)*s1;
	str2 = (unsigned long *)*s2;
	while (*n > 7)
	{
		if (str1 != str2)
		{
			i = 8;
			while (--i >= 0)
				if (*((unsigned char *)str1 + i) !=
					*((unsigned char *)str2 + i))
					return (*((unsigned char *)str1 + i) -
							*((unsigned char *)str2 + i));
		}
		++str1;
		++str2;
		*n -= 8;
	}
	*s1 = (void*)str1;
	*s2 = (void*)str2;
	return (0);
}

static int	rstd_memcmp_terminate(const void *s1, const void *s2, size_t n)
{
	unsigned char	*str1;
	unsigned char	*str2;

	str1 = (unsigned char *)s1;
	str2 = (unsigned char *)s2;
	while (n--)
	{
		if (*str1 != *str2)
			return (*str1 - *str2);
		++str1;
		++str2;
	}
	return (0);
}

int			rstd_memcmp(const void *s1, const void *s2, size_t n)
{
	int		result;

	if (n == 0)
		return (0);
	result = 0;
	if ((unsigned long)s1 % sizeof(void *))
		if ((result = rstd_memcmp_align(&s1, &s2, &n)))
			return (result);
	if (n > 7)
		if ((result = rstd_memcmp_bulk(&s1, &s2, &n)))
			return (result);
	if (n > 0)
		if ((result = rstd_memcmp_terminate(s1, s2, n)))
			return (result);
	return (result);
}
