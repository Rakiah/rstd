/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_atof.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:10 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/28 23:45:33 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

float		rstd_atof(const char *str)
{
	float	value;
	int		sign;
	int		i;

	value = 0;
	sign = (*str == '-') ? -1 : 1;
	if (*str == '-' || *str == '+')
		str++;
	while (rstd_isdigit(*str))
	{
		value = value * 10 + (*str - '0');
		str++;
	}
	if (*str == '.')
	{
		str++;
		i = 1;
		while (rstd_isdigit(*str))
		{
			value += (float)(*str - '0') / pow(10, i++);
			str++;
		}
	}
	return (value * sign);
}
