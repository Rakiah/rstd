/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_strdup.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:14 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/29 00:40:22 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

char	*rstd_strdup(const char *src)
{
	char	*allocated_ptr;

	allocated_ptr = rstd_strnew(rstd_strlen(src));
	rstd_strcpy(allocated_ptr, src);
	return (allocated_ptr);
}
