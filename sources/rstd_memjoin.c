/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_memjoin.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:11 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/28 23:45:35 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

void	*rstd_memjoin(void *c1, void *c2, size_t size_c1, size_t size_c2)
{
	void *realloc;

	if (size_c1 + size_c2 == 0)
		return (NULL);
	realloc = malloc(size_c1 + size_c2);
	rstd_memcpy(realloc, c1, size_c1);
	rstd_memcpy(realloc + size_c1, c2, size_c2);
	return (realloc);
}
