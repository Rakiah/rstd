/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_strsplit.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:16 by Rakiah            #+#    #+#             */
/*   Updated: 2016/07/07 19:24:53 by bkabbas          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

static char		**alloc_char_array(char *src,
									int bounds[5],
									int *size,
									char c)
{
	char	**ret;
	int		length;
	int		i;
	int		next_start;

	i = 0;
	ret = (char **)rstd_memalloc((bounds[3] + 1) * sizeof(char **));
	if (size != NULL)
		(*size) = bounds[3];
	next_start = bounds[0];
	while (i < bounds[3])
	{
		length = rstd_strchrnul(src + next_start, (int)c) - (src + next_start);
		if (length <= 0)
			break ;
		ret[i] = rstd_strsub(src, next_start, length);
		next_start = rstd_getnextxchar(src, (int)c, next_start + length, +1);
		i++;
	}
	return (ret);
}

char			**rstd_strsplit(char const *s, char c, int *size)
{
	int			bounds[5];
	char		**ret;

	bounds[2] = rstd_strlen(s);
	bounds[0] = rstd_getnextxchar(s, (int)c, 0, +1);
	bounds[1] = rstd_getnextxchar(s, (int)c, bounds[2] - 1, -1);
	if (bounds[0] > bounds[1])
	{
		ret = (char **)rstd_memalloc((1) * sizeof(char **));
		if (size != NULL)
			*size = 0;
		return (ret);
	}
	bounds[3] = 1;
	bounds[4] = bounds[0];
	while (bounds[4] < bounds[1])
	{
		if (s[bounds[4]] == c)
		{
			bounds[3]++;
			bounds[4] = rstd_getnextxchar(s, (int)c, bounds[4], +1);
		}
		bounds[4]++;
	}
	return (alloc_char_array((char *)s, bounds, size, c));
}

static char		**realloc_line(const char *s, char **ret, int l1, int l2)
{
	char	*line;
	char	**tmp;

	line = rstd_strndup(s, l1);
	tmp = rstd_memjoin(ret, &line, l2 * sizeof(char *), sizeof(char *));
	free(ret);
	return (tmp);
}

char			**rstd_strsplit_all(const char *s, char c, int *size)
{
	char	**ret;
	int		start;
	int		current;
	int		split_size;

	start = 0;
	current = 0;
	ret = NULL;
	split_size = 0;
	while (s[current] != '\0')
	{
		if (s[current] == c)
		{
			ret = realloc_line(s + start, ret, current - start, split_size);
			start = current + 1;
			split_size++;
		}
		current++;
	}
	if (size != NULL)
		*size = split_size;
	return (ret);
}
