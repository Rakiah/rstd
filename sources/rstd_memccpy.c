/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_memccpy.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:11 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/29 00:36:58 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

void	*rstd_memccpy(void *dest, const void *src, int c, size_t n)
{
	size_t	size;
	char	*source;

	if (n == 0)
		return (NULL);
	source = (char *)src;
	size = 0;
	while (source[size] != (char)c)
	{
		++size;
		if (size == n)
		{
			rstd_memcpy(dest, src, n);
			return (NULL);
		}
	}
	rstd_memcpy(dest, src, size);
	return ((void *)((char *)dest + size));
}
