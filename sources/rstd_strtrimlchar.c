/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_strtrimlchar.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:17 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/29 00:46:29 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

char		*rstd_strtrimlchar(char const *s, int rm)
{
	char		*str_new;
	int			start;
	int			length;

	start = rstd_getnextxchar(s, rm, 0, +1);
	length = rstd_strlen(s) - start;
	str_new = rstd_strnew(length);
	rstd_strncpy(str_new, s + start, length);
	return (str_new);
}
