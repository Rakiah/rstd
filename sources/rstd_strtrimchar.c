/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_strtrimchar.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:16 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/29 00:46:23 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

char		*rstd_strtrimchar(char const *s, int rm)
{
	char		*str_new;
	int			start;
	int			end;
	int			length;

	start = rstd_getnextxchar(s, rm, 0, +1);
	end = rstd_getnextxchar(s, rm, rstd_strlen(s) - 1, -1);
	if (start > end)
		return (rstd_strnew(0));
	length = end - start;
	str_new = rstd_strnew(length);
	rstd_strncpy(str_new, s + start, length + 1);
	return (str_new);
}
