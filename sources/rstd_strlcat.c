/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_strlcat.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:15 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/28 23:45:39 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

size_t	rstd_strlcat(char *dest, const char *src, size_t size)
{
	char	*d;
	char	*s;
	size_t	diff;
	size_t	dlen;

	s = (char *)src;
	d = dest;
	diff = size;
	while (diff-- != 0 && *d != '\0')
		d++;
	dlen = d - dest;
	diff = size - dlen;
	if (diff == 0)
		return (dlen + rstd_strlen(s));
	while (*s != '\0')
	{
		if (diff != 1)
		{
			*d++ = *s;
			diff--;
		}
		s++;
	}
	*d = 0;
	return (dlen + (s - src));
}
