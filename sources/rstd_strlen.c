/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_strlen.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:15 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/28 23:45:39 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

size_t		rstd_strlen(char const *str)
{
	size_t length;

	length = 0;
	while (*str++)
		length++;
	return (length);
}
