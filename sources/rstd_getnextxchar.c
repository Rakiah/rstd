/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_getnextxchar.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:10 by Rakiah            #+#    #+#             */
/*   Updated: 2016/07/07 14:18:23 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** this function search for the next char that is not c, starting by start
** index the move parameter should be valid or undetermined behaviour could
** happen you will probably always want to have -1 or +1 but for control
** purposes i decided to leave the choice, if you want to skip some
** characters the function return the index of the excluded char
*/

#include "rstd.h"

int	rstd_getnextxchar(const char *s, int c, int start, int move)
{
	while (s[start] == (unsigned char)c && start >= 0)
		start += move;
	return (start);
}
