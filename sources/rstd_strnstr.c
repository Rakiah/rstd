/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_strnstr.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:16 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/29 00:43:37 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

char	*rstd_strnstr(const char *str, const char *to_find, size_t length)
{
	const char	*to_finditer;
	const char	*striter;

	if (*to_find == '\0')
		return ((char *)str);
	while (*str != '\0' && length > 0)
	{
		if (*str == *to_find)
		{
			striter = str;
			to_finditer = to_find;
			while (*striter == *to_finditer &&
					length - (to_finditer - to_find) > 0)
			{
				(striter)++;
				(to_finditer)++;
				if (*to_finditer == '\0')
					return ((char *)str);
			}
		}
		(str)++;
		length--;
	}
	return (NULL);
}
