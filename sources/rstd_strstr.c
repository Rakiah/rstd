/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_strstr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:16 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/28 23:45:41 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

char	*rstd_strstr(const char *str, const char *to_find)
{
	const char	*to_finditer;
	const char	*striter;

	if (*to_find == '\0')
		return ((char *)str);
	while (*str != '\0')
	{
		if (*str == *to_find)
		{
			striter = str;
			to_finditer = to_find;
			while (*striter == *to_finditer)
			{
				(striter)++;
				(to_finditer)++;
				if (*to_finditer == '\0')
					return ((char *)str);
			}
		}
		(str)++;
	}
	return (NULL);
}
