/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_strtolower.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:16 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/29 00:45:53 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

char	*rstd_strtolower(char *s)
{
	char *start;

	start = s;
	while (*s)
	{
		if (rstd_isupper((int)*s))
			(*s) += 'a' - 'A';
		(s)++;
	}
	return (start);
}
