/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_delete_tab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:10 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/29 00:32:16 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

void	rstd_delete_tab(void **tab, int height)
{
	int i;

	i = -1;
	while (++i < height)
		DELETE(tab[i]);
	DELETE(tab);
}
