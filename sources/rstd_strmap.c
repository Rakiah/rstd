/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_strmap.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:15 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/29 00:42:19 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

char	*rstd_strmap(char const *s, char (*f)(char))
{
	char	*ret_str;
	int		i;

	ret_str = rstd_strnew(rstd_strlen(s) + 1);
	i = 0;
	while (s[i])
	{
		ret_str[i] = f(s[i]);
		i++;
	}
	return (ret_str);
}
