/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_memalloc.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:11 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/29 00:36:47 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

/*
** Standard 7.20.3: malloc(0)
** If the size of the space requested is zero,
** the behavior is implementation- deﬁned: either a null pointer is returned,
** or the behavior is as if the size were some nonzero value,
** except that the returned pointer shall not be used to access an object.
*/

void	*rstd_memalloc(size_t size)
{
	void	*p;

	if (size == 0)
		return (NULL);
	p = malloc(size);
	rstd_bzero(p, size);
	return (p);
}
