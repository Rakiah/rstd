/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_memswap.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:12 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/28 23:45:35 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

void	rstd_memswap(void *swpf, void *swps)
{
	char tmp;

	tmp = *(char *)swpf;
	*(char *)swpf = *(char *)swps;
	*(char *)swps = tmp;
}
