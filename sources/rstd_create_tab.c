/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_create_tab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:10 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/29 00:31:40 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

void	**rstd_create_tab(int width, int height, size_t element_size)
{
	void	**ret;
	int		i;

	i = -1;
	ret = (void **)malloc(width * sizeof(void *));
	while (++i < width)
		ret[i] = (void *)malloc(height * element_size);
	return (ret);
}
