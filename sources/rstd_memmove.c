/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_memmove.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:12 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/28 23:45:35 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

static void	rstd_memmove_align(void **dest, const void **src, size_t *n)
{
	size_t			size;
	unsigned char	*destination;
	unsigned char	*source;

	destination = (unsigned char *)*dest + *n;
	source = (unsigned char *)*src + *n;
	size = (unsigned long int)destination % sizeof(void *);
	if (*n < size)
		size = *n;
	*n -= size;
	while (size)
	{
		--destination;
		--source;
		*destination = *source;
		--size;
	}
	*dest = (void *)destination;
	*src = (void *)source;
}

static void	rstd_memmove_bulk(void **dest, const void **src, size_t *n)
{
	unsigned char	swap[4096];
	unsigned char	*destination;
	unsigned char	*source;

	destination = (unsigned char *)*dest;
	source = (unsigned char *)*src;
	while (*n >= 4096)
	{
		destination -= 4096;
		source -= 4096;
		rstd_memcpy(swap, source, 4096);
		rstd_memcpy(destination, swap, 4096);
		*n -= 4096;
	}
	*dest = (void *)destination;
	*src = (void *)source;
}

static void	rstd_memmove_terminate(void **dest, const void **src, size_t *n)
{
	unsigned char	swap[*n];
	unsigned char	*destination;
	unsigned char	*source;

	destination = (unsigned char *)*dest - *n;
	source = (unsigned char *)*src - *n;
	rstd_memcpy(swap, source, *n);
	rstd_memcpy(destination, swap, *n);
	*n -= 0;
	*dest = (void *)destination;
	*src = (void *)source;
}

void		*rstd_memmove(void *dest, const void *src, size_t n)
{
	void	*orig;

	if (n == 0 || dest == src)
		return (dest);
	if (src > dest)
		return (rstd_memcpy(dest, src, n));
	orig = dest;
	rstd_memmove_align(&dest, &src, &n);
	if (n >= 4096)
		rstd_memmove_bulk(&dest, &src, &n);
	if (n != 0)
		rstd_memmove_terminate(&dest, &src, &n);
	return (orig);
}
