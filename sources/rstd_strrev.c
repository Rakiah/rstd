/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_strrev.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:16 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/29 00:43:55 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

char	*rstd_strrev(char *s)
{
	char	*start;
	size_t	length;
	size_t	i;

	start = s;
	length = rstd_strlen(s);
	i = 0;
	while (i < length / 2)
	{
		rstd_memswap((void *)(s + i), (void *)(s + (length - i - 1)));
		i++;
	}
	return (start);
}
