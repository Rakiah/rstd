/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_read_line.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/29 00:57:56 by Rakiah            #+#    #+#             */
/*   Updated: 2016/07/07 19:25:31 by bkabbas          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rstd.h>
#include <fcntl.h>

static void	*read_all(int fd)
{
	char	stack[READ_SIZE + 1];
	char	*buffer;
	char	*tmp;
	int		buffer_size;
	int		read_count;

	buffer_size = 0;
	buffer = NULL;
	while ((read_count = read(fd, stack, READ_SIZE)) > 0)
	{
		stack[READ_SIZE] = '\0';
		tmp = rstd_memjoin(buffer, stack, buffer_size, read_count + 1);
		buffer_size += read_count;
		free(buffer);
		buffer = tmp;
	}
	return (buffer);
}

int			rstd_read_lines(const char *file, char ***lines)
{
	int		fd;
	char	*buffer;
	int		split_size;

	if ((fd = open(file, O_RDONLY)) < 0)
		return (-1);
	buffer = read_all(fd);
	*lines = rstd_strsplit_all(buffer, '\n', &split_size);
	return (split_size);
}
