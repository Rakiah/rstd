/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_strncpy.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:15 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/28 23:45:40 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

char	*rstd_strncpy(char *dst, const char *src, size_t n)
{
	char	*firstocc;

	firstocc = dst;
	while (n > 0 && *src)
	{
		*dst = *src;
		(dst)++;
		(src)++;
		n--;
	}
	while (n > 0)
	{
		*dst = '\0';
		(dst)++;
		n--;
	}
	return (firstocc);
}
