/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_strchr_count.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:14 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/29 00:39:49 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

int		rstd_strchr_count(const char *s, int c)
{
	int		count;

	count = 0;
	while (*s)
	{
		if (*s == (unsigned char)c)
			count++;
		(s)++;
	}
	return (count);
}
