/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd_strtrim.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:33:16 by Rakiah            #+#    #+#             */
/*   Updated: 2016/06/29 00:46:13 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

char	*rstd_strtrim(char const *s)
{
	char	*str_new;
	int		start;
	int		end;
	int		length;

	start = 0;
	while (s[start] &&
			(s[start] == '\t' || s[start] == ' ' || s[start] == '\n'))
		start++;
	end = rstd_strlen(s) - 1;
	if (start > end)
		return (rstd_strnew(0));
	while (s[end] &&
			(s[end] == '\t' || s[end] == ' ' || s[end] == '\n'))
		end--;
	end++;
	length = end - start;
	str_new = rstd_strnew(length);
	rstd_strncpy(str_new, s + start, length);
	return (str_new);
}
