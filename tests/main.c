/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:54:28 by Rakiah            #+#    #+#             */
/*   Updated: 2016/07/07 01:00:32 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rstd.h>
#include <stdio.h>

typedef struct s_test
{
	int t1;
	int t2;
	int t3;
} t_test;

int main()
{
	t_test *t;

	t = NEW(t_test);
	t->t1 = 0;
	t->t2 = 0;
	t->t3 = 0;
	DELETE(t);
	printf("%p\n", t);
	return (0);
}
