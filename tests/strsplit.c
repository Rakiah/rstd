/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strsplit.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/07 14:56:54 by Rakiah            #+#    #+#             */
/*   Updated: 2016/07/07 15:58:29 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rstd.h"

int		main(int ac, char **av)
{
	int i;
	int	split_size;
	char **split;


	(void)ac;
	i = 0;
	split = rstd_strsplit(av[1], ' ', &split_size);
	/*rstd_putnbr(split_size);*/
	(void)split;
	while (i < split_size)
	{
		rstd_putnbr(i);
		rstd_putchar(' ');
		rstd_putendl(split[i]);
		i++;
	}
	return (0);
}
