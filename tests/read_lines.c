/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_lines.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/07 01:01:02 by Rakiah            #+#    #+#             */
/*   Updated: 2016/07/07 16:03:27 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rstd.h>

int main(int ac, char **av)
{
	char	**lines;
	int		i;
	int		count;

	(void)ac;
	i = 0;
	if ((count = rstd_read_lines(av[1], &lines)) < 0)
	{
		rstd_putstr("ERROR");
		exit (0);
	}
	while (i < count)
		rstd_putendl(lines[i++]);
	free(lines);
	return (0);
}
