/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rstd.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Rakiah <bkabbas@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/28 23:40:18 by Rakiah            #+#    #+#             */
/*   Updated: 2016/08/04 23:14:18 by Rakiah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RSTD_H
# define RSTD_H

# include <stdlib.h>
# include <string.h>
# include <unistd.h>
# include <math.h>

# define NEW(type) (type *)malloc(sizeof(type))
# define DELETE(ptr) free(ptr); ptr = NULL
# define READ_SIZE 4096

typedef enum { false, true } t_bool;

void		*rstd_memset(void *dst, int c, size_t s_bytes);
void		*rstd_memcpy(void *dst, const void *s, size_t s_bytes);
void		*rstd_memccpy(void *dst, const void *s, int c, size_t b);
void		*rstd_memmove(void *dst, const void *src, size_t s_bytes);
void		*rstd_memchr(const void *src, int c, size_t s_bytes);
int			rstd_memcmp(const void *s1, const void *s2, size_t b);
void		rstd_bzero(void *dst, size_t s_bytes);
int			rstd_atoi(const char *str);
float		rstd_atof(const char *str);
size_t		rstd_strlen(char const *str);
int			rstd_strcmp(const char *s1, const char *s2);
int			rstd_strncmp(const char *s1, const char *s2, size_t l);
char		*rstd_strcpy(char *dst, const char *src);
char		*rstd_strdup(const char *src);
char		*rstd_strndup(const char *src, size_t n);
char		*rstd_strncpy(char *dst, const char *src, size_t n);
char		*rstd_strcat(char *dst, const char *src);
char		*rstd_strncat(char *dst, const char *src, size_t n);
char		*rstd_strchr(const char *s, int c);
char		*rstd_strrchr(const char *s, int c);
char		*rstd_strstr(const char *str, const char *to_find);
char		*rstd_strnstr(const char *s, const char *t, size_t l);
size_t		rstd_strlcpy(char *dest, const char *src, size_t size);
size_t		rstd_strlcat(char *dest, const char *src, size_t size);
int			rstd_isalnum(int c);
int			rstd_isalpha(int c);
int			rstd_isascii(int c);
int			rstd_isdigit(int c);
int			rstd_isprint(int c);
int			rstd_isspace(int c);
int			rstd_toupper(int c);
int			rstd_tolower(int c);
char		*rstd_strnew(size_t size);
void		*rstd_memalloc(size_t size);
void		rstd_memdel(void **ap);
void		rstd_strdel(char **as);
void		rstd_strclr(char *s);
void		rstd_striter(char *s, void (*f)(char *));
void		rstd_striteri(char *s, void (*f)(unsigned int, char *));
char		*rstd_strmap(char const *s, char (*f)(char));
char		*rstd_strmapi(char const *s, char(*f)(unsigned int, char));
char		*rstd_strsub(char const *s, unsigned int start, size_t len);
char		*rstd_strtrim(char const *s);
char		*rstd_strjoin(char const *s1, char const *s2);
char		**rstd_strsplit(char const *s, char c, int *size);
char		**rstd_strsplit_all(char const *s, char c, int *size);
int			rstd_strequ(char const *s1, char const *s2);
int			rstd_strnequ(char const *s1, char const *s2, size_t n);
char		*rstd_itoa(int n);
void		rstd_putchar(char c);
void		rstd_putchar_fd(char c, int fd);
void		rstd_putstr(char const *s);
void		rstd_putstr_fd(char const *s, int fd);
void		rstd_putendl(char const *s);
void		rstd_putendl_fd(char const *s, int fd);
void		rstd_putnbr(int n);
void		rstd_putnbr_fd(int n, int fd);
void		rstd_memswap(void *swpf, void *swps);
void		*rstd_memjoin(void *c1, void *c2, size_t sc1, size_t sc2);
char		*rstd_strtrimchar(char const *s, int rm);
char		*rstd_strtrimlchar(char const *s, int rm);
char		*rstd_strtrimrchar(char const *s, int rm);
char		*rstd_strrev(char *s);
char		*rstd_strchrnul(const char *s, int c);
char		*rstd_strtoupper(char *s);
char		*rstd_strtolower(char *s);
int			rstd_strchr_count(const char *s, int c);
char		*rstd_strnchr(const char *s, int c, int n);
int			rstd_getnextxchar(const char *s, int c, int start, int move);
int			rstd_isspace(int c);
int			rstd_islower(int c);
int			rstd_isupper(int c);
void		**rstd_create_tab(int width, int height, size_t element_size);
void		rstd_delete_tab(void **tab, int height);
int			rstd_read_lines(const char *file, char ***lines);

#endif
